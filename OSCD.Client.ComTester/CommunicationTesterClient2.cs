﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace OSCD.Client.ComTester
{
    class CommunicationTesterClient2 : CommunicationTesterClient
    {
        private int streamMessageCounter = 0;

        public CommunicationTesterClient2(IPEndPoint oscdEndPoint) : base(oscdEndPoint)
        {
        }

        protected override void StartTester()
        {
            Console.WriteLine("---===== OSCDClient Communication Tester 2 =====---");

            subjects.Add("C2S1", "/Client2/Subject1");

            Connect();
            PrepareClient();

            Stream();

            Disconnect();
        }

        protected override void PrepareClient()
        {
            Console.WriteLine($"RegisterUser: {client.RegisterUser(OnRegisterUser)}");
        }

        protected new void OnRegisterUser(object[] data)
        {
            base.OnRegisterUser(data);

            Console.WriteLine($"RegisterSubject: {client.RegisterSubject(OnRegisterSubject, subjects["C2S1"])}");
        }

        protected override async Task Stream()
        {
            Console.WriteLine("\nPress 'ENTER' to start streaming.");
            Console.ReadLine();

            await Task.Run(() =>
            {
                while (IsStreamLooping)
                {
                    if (client.Publish(subjects["C2S1"], random.NextDouble(), random.Next(), random.NextDouble() > .5f, subjects["C2S1"]))
                        Console.WriteLine($"(2 Sent)   ({++streamMessageCounter})");
                    else
                        Console.WriteLine($"(2 Error)   ({streamMessageCounter})");
                }
            });
        }
    }
}
