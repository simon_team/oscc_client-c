﻿using System;
using System.Collections.Generic;
using System.Net;

namespace OSCD.Client.ComTester
{
    abstract class TesterClient
    {
        public bool IsDone { get; private set; }

        protected static readonly object ConsoleWriterLock = new object();

        protected OscdClient client;
        protected Dictionary<string, string> subjects;

        protected TesterClient(IPEndPoint oscdEndPoint)
        {
            client = new OscdClient(oscdEndPoint);
            client.OnSubjectCancellationReceived += OnSubjectCancellationReceived;
            client.OnSubjectNotificationReceived += OnSubjectNotificationReceived;

            subjects = new Dictionary<string, string>();
        }

        public void Start()
        {
            if (IsDone)
                throw new InvalidOperationException();

            IsDone = true;

            StartTester();
        }

        protected abstract void StartTester();

        protected void Connect()
        {
            Console.WriteLine("\nPress 'ENTER' to connect.");
            Console.ReadLine();
            Console.WriteLine($"Connect: {client.Connect()}");
        }

        protected void Disconnect()
        {
            Console.WriteLine("\nPress 'ENTER' to disconnect.");
            Console.ReadLine();
            Console.WriteLine($"Disconnect: {client.Disconnect()}");
        }

        #region Alert Handlers
        private void OnSubjectCancellationReceived(object sender, SubjectAlertReceivedEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine($"The subject \"{e.Subject}\" was cancelled");
            Console.ResetColor();
        }

        private void OnSubjectNotificationReceived(object sender, SubjectAlertReceivedEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine($"The subject \"{e.Subject}\" is now available");
            Console.ResetColor();
        }
        #endregion

        #region Response Handlers
        protected void OnRegisterUser(object[] data)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;

            Console.WriteLine("(OnRegisterUser) Received data:");
            for (int idx = 0; idx < data.Length; idx++)
                Console.WriteLine($"   [{idx}]: {data[idx]}");

            Console.ResetColor();
        }

        protected void OnRegisterSubject(object[] data)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;

            Console.WriteLine("(OnRegisterSubject) Received data:");
            for (int idx = 0; idx < data.Length; idx++)
                if (data[idx].GetType().IsArray)
                {
                    KeyValuePair<string, bool>[] pairs = (KeyValuePair<string, bool>[])data[idx];
                    Console.WriteLine($"   [{idx}]: [");
                    for (int iidx = 0; iidx < pairs.Length; iidx++)
                        Console.WriteLine($"           [{iidx}]: {pairs[iidx]}");
                    Console.WriteLine($"        ]");
                }
                else
                    Console.WriteLine($"   [{idx}]: {data[idx]}");

            Console.ResetColor();
        }

        protected void OnGetAvailableSubjects(object[] data)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;

            Console.WriteLine("(OnGetAvailableSubjects) Received data:");
            for (int idx = 0; idx < data.Length; idx++)
                if (data[idx].GetType().IsArray)
                {
                    string[] subjects = (string[])data[idx];
                    Console.WriteLine($"   [{idx}]: [");
                    for (int iidx = 0; iidx < subjects.Length; iidx++)
                        Console.WriteLine($"           [{iidx}]: {subjects[iidx]}");
                    Console.WriteLine($"        ]");
                }
                else
                    Console.WriteLine($"   [{idx}]: {data[idx]}");

            Console.ResetColor();
        }

        protected void OnSubscribeSubject(object[] data)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;

            Console.WriteLine("(OnSubscribeSubject) Received data:");
            for (int idx = 0; idx < data.Length; idx++)
                if (data[idx].GetType().IsArray)
                {
                    KeyValuePair<string, bool>[] pairs = (KeyValuePair<string, bool>[])data[idx];
                    Console.WriteLine($"   [{idx}]: [");
                    for (int iidx = 0; iidx < pairs.Length; iidx++)
                        Console.WriteLine($"           [{iidx}]: {pairs[iidx]}");
                    Console.WriteLine($"        ]");
                }
                else
                    Console.WriteLine($"   [{idx}]: {data[idx]}");

            Console.ResetColor();
        }
        #endregion
    }
}
