﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace OSCD.Client.ComTester
{
    class CommunicationTesterClient4 : CommunicationTesterClient
    {
        private int onSubscriptionReceived_3_Counter = 0;

        public CommunicationTesterClient4(IPEndPoint oscdEndPoint) : base(oscdEndPoint)
        {
        }

        protected override void StartTester()
        {
            Console.WriteLine("---===== OSCDClient Communication Tester 3 =====---");

            subjects.Add("C1S1", "/Client1/Subject1");
            subjects.Add("C1S2", "/Client1/Subject2");
            subjects.Add("C1S4", "/Client1/Subject4");

            Connect();
            PrepareClient();

            Stream();

            Disconnect();
        }

        protected override void PrepareClient()
        {
            Console.WriteLine($"RegisterUser: {client.RegisterUser(OnRegisterUser)}");

        }

        protected new void OnRegisterUser(object[] data)
        {
            base.OnRegisterUser(data);

            OscdSubscription c1Subscription1 = new OscdSubscription(subjects["C1S1"], OnSubscriptionReceived_3, false);
            OscdSubscription c1Subscription2 = new OscdSubscription(subjects["C1S2"], OnSubscriptionReceived_3, false);
            Console.WriteLine($"SubscribeSubject: {client.SubscribeSubject(OnSubscribeSubject, c1Subscription1, c1Subscription2)}");
        }

        protected override Task Stream()
        {
            return null;
        }

        private void OnSubscriptionReceived_3(object[] data)
        {
            lock (ConsoleWriterLock)
            {
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
                Console.WriteLine($"(Received)   ({++onSubscriptionReceived_3_Counter})");
                Console.ResetColor();
            }
        }
    }
}
