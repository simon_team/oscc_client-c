﻿using Bespoke.Common.Osc.Unity;

namespace OSCD.Client.Unity
{
    class CommandHandlerData
    {
        public int commandId;
        public OscMessage command;
        public MessageDataHandler handler;
    }
}
