﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace OSCD.Client.ComTester
{
    abstract class CommunicationTesterClient : TesterClient
    {
        protected bool IsStreamLooping = true;
        protected int nMessagesSent = 0;

        protected Random random = new Random();

        public CommunicationTesterClient(IPEndPoint oscdEndPoint) : base(oscdEndPoint)
        {
        }

        protected abstract void PrepareClient();

        protected abstract Task Stream();

        protected new void Disconnect()
        {
            base.Disconnect();
            Stop();
        }

        public void Stop()
        {
            IsStreamLooping = false;
        }
    }
}
