﻿using Bespoke.Common.Osc;

namespace OSCD.Client
{
    class CommandHandlerData
    {
        public int commandId;
        public OscMessage command;
        public MessageDataHandler handler;
    }
}
