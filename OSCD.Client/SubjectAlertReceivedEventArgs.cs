﻿
namespace OSCD.Client
{
    public class SubjectAlertReceivedEventArgs
    {
        public string Subject { get; private set; }

        public SubjectAlertReceivedEventArgs(string subject)
        {
            Subject = subject;
        }
    }
}
