﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace OSCD.Client.ComTester
{
    class CommunicationTesterClient3 : CommunicationTesterClient
    {
        private int streamMessageCounter = 0;

        public CommunicationTesterClient3(IPEndPoint oscdEndPoint) : base(oscdEndPoint)
        {
        }

        protected override void StartTester()
        {
            Console.WriteLine("---===== OSCDClient Communication Tester 3 =====---");

            subjects.Add("C3S1", "/Client3/Subject1");

            Connect();
            PrepareClient();

            Stream();

            Disconnect();
        }

        protected override void PrepareClient()
        {
            Console.WriteLine($"RegisterUser: {client.RegisterUser(OnRegisterUser)}");
        }

        protected new void OnRegisterUser(object[] data)
        {
            base.OnRegisterUser(data);

            Console.WriteLine($"RegisterSubject: {client.RegisterSubject(OnRegisterSubject, subjects["C3S1"])}");
        }

        protected override async Task Stream()
        {
            Console.WriteLine("\nPress 'ENTER' to start streaming.");
            Console.ReadLine();

            await Task.Run(() =>
            {
                while (IsStreamLooping)
                {
                    if (client.Publish(subjects["C3S1"], random.NextDouble(), random.Next(), random.NextDouble() > .5f, subjects["C3S1"]))
                        Console.WriteLine($"(3 Sent)   ({++streamMessageCounter})");
                    else
                        Console.WriteLine($"(3 Error)   ({streamMessageCounter})");
                }
            });
        }
    }
}
