﻿using System;
using System.IO;
using System.Text;

namespace OSCD.Client.ComTester
{
    class ErrorWriter : TextWriter
    {
        public const ConsoleColor color = ConsoleColor.Red;

        public override Encoding Encoding
        {
            get { return Encoding.Default; }
        }

        public override void Write(string value)
        {
            Console.ForegroundColor = color;
            Console.Write(value);
            Console.ResetColor();
        }

        public override void WriteLine(string value)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(value);
            Console.ResetColor();
        }
    }
}
