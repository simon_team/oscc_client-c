﻿using Bespoke.Common.Net;
using Bespoke.Common.Osc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using Bespoke.Common;

namespace OSCD.Client
{
    public class OscdClient
    {
        #region OSCD Attributes
        public const string RegisterUserAddress = "/OSCD/RegisterUser";
        public const string RegisterSubjectAddress = "/OSCD/RegisterSubject";
        public const string UnregisterSubjectAddress = "/OSCD/UnregisterSubject";
        public const string GetAvailableSubjectsAddress = "/OSCD/GetAvailableSubjects";
        public const string EnableSubjectNotificationAddress = "/OSCD/EnableSubjectNotification";
        public const string DisableSubjectNotificationAddress = "/OSCD/DisableSubjectNotification";
        public const string SubscribeSubjectAddress = "/OSCD/SubscribeSubject";
        public const string UnsubscribeSubjectAddress = "/OSCD/UnsubscribeSubject";

        public const string ResponseAddress = "/OSCD/Response";
        public const string ErrorAddress = "/OSCD/Error";
        public const string SubjectCancellationAddress = "/OSCD/SubjectCancellation";
        public const string SubjectNotificationAddress = "/OSCD/SubjectNotification";

        private IPEndPoint oscdEndPoint;

        private int commandCounter = 0;
        Dictionary<int, CommandHandlerData> pendingCommands;

        Dictionary<string, ushort> activeSubjects;
        Dictionary<string, MessageDataHandler> pendingSubscriptions;
        Dictionary<string, MessageDataHandler> activeSubscriptions;
        HashSet<string> activeNotifications;

        public event EventHandler<SubjectAlertReceivedEventArgs> OnSubjectNotificationReceived;
        public event EventHandler<SubjectAlertReceivedEventArgs> OnSubjectCancellationReceived;
        public event EventHandler<EventArgs> OnDisconnected;
        #endregion

        #region OSC Attributes
        private OscBiDirectionalClient commandClient;
        private OscServer subscriptionClient;

        public const string subjectRegexPattern = @"^(\/[A-Za-z0-9]+)+$";

        private string oscUserID;
        #endregion

        private void ResetClient()
        {
            oscUserID = null;
            activeSubjects.Clear();
            pendingSubscriptions.Clear();
            activeSubscriptions.Clear();
            activeNotifications.Clear();
        }

        public bool IsConnected
        {
            get
            {
                return commandClient != null && commandClient.IsConnected && subscriptionClient != null && subscriptionClient.IsRunning;
            }
        }

        private int GetNextCommandID
        {
            get
            {
                if (++commandCounter < 0)
                    commandCounter = 1;

                return commandCounter;
            }
        }

        #region Initialization
        public OscdClient(IPEndPoint oscdEndPoint)
        {
            this.oscdEndPoint = oscdEndPoint;

            InstantiateCommandClient(oscdEndPoint);
            InstantiateSubscriptionClient();

            pendingCommands = new Dictionary<int, CommandHandlerData>();

            activeSubjects = new Dictionary<string, ushort>();
            pendingSubscriptions = new Dictionary<string, MessageDataHandler>();
            activeSubscriptions = new Dictionary<string, MessageDataHandler>();
            activeNotifications = new HashSet<string>();
        }

        private void InstantiateCommandClient(IPEndPoint oscdEndPoint)
        {
            commandClient = new OscBiDirectionalClient(oscdEndPoint);
            commandClient.FilterRegisteredMethods = true;
            commandClient.MessageReceived += CommandResponseReceived;
            commandClient.ReceiveErrored += CommandReceiveErrored;
            commandClient.Disconnected += ClientDisconnected;

            commandClient.RegisterMethod(ResponseAddress);
            commandClient.RegisterMethod(ErrorAddress);
            commandClient.RegisterMethod(SubjectCancellationAddress);
            commandClient.RegisterMethod(SubjectNotificationAddress);
        }

        private void InstantiateSubscriptionClient()
        {
            subscriptionClient = new OscServer(TransportType.Udp, IPAddress.Any, 0, false);
            subscriptionClient.FilterRegisteredMethods = true;
            subscriptionClient.MessageReceived += PublicationReceived;
            subscriptionClient.ReceiveErrored += PublicationReceiveErrored;
        }
        #endregion

        public bool Publish(string subject, params object[] data)
        {
            // Tests if the client is registered
            if (oscUserID == null)
                return false;

            // Tests if the received subject is registered
            if (!activeSubjects.TryGetValue(subject, out ushort port))
                return false;

            // Creates and sends the message
            OscMessage message = new OscMessage(null, subject, oscUserID);
            foreach (object item in data)
                try
                {
                    message.Append(item);
                }
                catch (Exception)
                {
                    throw new ArgumentException("Some of the received data is unsupported.");
                }

            message.Send(new IPEndPoint(oscdEndPoint.Address, port));
            return true;
        }

        private void PublicationReceived(object sender, OscMessageReceivedEventArgs e)
        {
            if (!activeSubscriptions.TryGetValue(e.Message.Address, out MessageDataHandler handler))
                Console.Error.WriteLine($"The received message has the unknown subject \"{e.Message.Address}\".");
            else
                handler.Invoke(e.Message.Data.ToArray());
        }

        #region Connection Handlers
        public bool Connect()
        {
            if (commandClient.IsConnected)
                return false;

            try
            {
                commandClient.Connect();
                subscriptionClient.Start();
            }
            catch (SocketException e)
            {
                Console.Error.WriteLine($"{e.GetType()}: {e.Message}");
                return false;
            }

            return true;
        }

        public bool Disconnect()
        {
            if (!commandClient.IsConnected)
                return false;

            commandClient.Close();
            subscriptionClient.Stop();

            ResetClient();
            return true;
        }

        private void ClientDisconnected(object sender, TcpConnectionEventArgs e)
        {
#if DEBUG
            Console.Error.WriteLine($"Disconnected");
#endif

            OnDisconnected?.Invoke(this, new EventArgs());
        }
        #endregion

        #region Command Senders
        private OscMessage GetNewCommandMessage(string messageAddress, bool appendUserId = true)
        {
            OscMessage commandMessage = new OscMessage(null, messageAddress, GetNextCommandID);
            if (appendUserId)
                commandMessage.Append(oscUserID);
            return commandMessage;
        }

        private bool SendCommand(OscMessage command, MessageDataHandler commandHandler = null)
        {
            // Tests if the client is connected
            if (!commandClient.IsConnected)
                return false;

            if (commandHandler != null)
            {
                // Creates the data object to be associated with the command
                CommandHandlerData handlerData = new CommandHandlerData
                {
                    commandId = (int)command.Data[0],
                    command = command,
                    handler = commandHandler
                };

                // Sends the command and adds it to the pending list
                commandClient.Send(command);
                pendingCommands.Add(handlerData.commandId, handlerData);

#if DEBUG
                Console.WriteLine($"Command sent with the id {handlerData.commandId}");
#endif
            }
            else
            {
                commandClient.Send(command);

#if DEBUG
                Console.WriteLine($"Command sent");
#endif
            }

            return true;
        }

        public bool RegisterUser(MessageDataHandler handler)
        {
            // Tests if the client is registered or if there already is a similar pending command
            if (oscUserID != null || pendingCommands.Any(command => command.Value.command.Address == RegisterUserAddress))
                return false;

            // Creates and sends the command
            return SendCommand(GetNewCommandMessage(RegisterUserAddress, false), handler);
        }

        public bool RegisterSubject(MessageDataHandler handler, params string[] subjects)
        {
            // Tests if the client is registered
            if (oscUserID == null)
                return false;

            // Filters the received subjects
            HashSet<string> subjectsSet = GetFilteredSubjects(subjects, RegisterSubjectAddress);

            if (subjectsSet.Count == 0)
                return false;

            // Creates and sends the command
            OscMessage commandMessage = GetNewCommandMessage(RegisterSubjectAddress);
            foreach (string subject in subjectsSet)
                commandMessage.Append(subject);

            return SendCommand(commandMessage, handler);
        }

        public bool UnregisterSubject(params string[] subjects)
        {
            // Tests if the client is registered 
            if (oscUserID == null)
                return false;

            // Gets a set of non-repetitive and valid subjects
            HashSet<string> subjectsSet = GetSubjectSet(subjects);

            // Excludes the received subjects that aren't activeNotifications
            subjectsSet.IntersectWith(activeSubjects.Keys);

            // Excludes the received subjects that are in pending commands
            FilterSubjectsByPendingCommand(subjectsSet, UnregisterSubjectAddress);

            if (subjectsSet.Count == 0)
                return false;

            // Creates and sends the command
            OscMessage commandMessage = GetNewCommandMessage(UnregisterSubjectAddress);
            foreach (string subject in subjectsSet)
            {
                activeSubjects.Remove(subject);
                commandMessage.Append(subject);
            }

            return SendCommand(commandMessage);
        }

        public bool GetAvailableSubjects(MessageDataHandler handler)
        {
            // Tests if there already is a similar pending command
            if (pendingCommands.Any(command => command.Value.command.Address == GetAvailableSubjectsAddress))
                return false;

            // Creates and sends the command
            return SendCommand(GetNewCommandMessage(GetAvailableSubjectsAddress, false), handler);
        }

        public bool EnableSubjectNotification(params string[] subjects)
        {
            // Tests if the client is registered 
            if (oscUserID == null)
                return false;

            // Filters the received subjects
            HashSet<string> subjectsSet = GetFilteredSubjects(subjects, EnableSubjectNotificationAddress);

            if (subjectsSet.Count == 0)
                return false;

            // Creates and sends the command
            OscMessage commandMessage = GetNewCommandMessage(EnableSubjectNotificationAddress);
            foreach (string subject in subjects)
            {
                activeNotifications.Add(subject);
                commandMessage.Append(subject);
            }

            return SendCommand(commandMessage);
        }

        public bool DisableSubjectNotification(params string[] subjects)
        {
            // Tests if the client is registered 
            if (oscUserID == null)
                return false;

            // Gets a set of non-repetitive and valid subjects
            HashSet<string> subjectsSet = GetSubjectSet(subjects);

            // Excludes the received subjects that aren't activeNotifications
            subjectsSet.IntersectWith(activeNotifications);

            // Excludes the received subjects that are in pending commands
            FilterSubjectsByPendingCommand(subjectsSet, DisableSubjectNotificationAddress);

            if (subjectsSet.Count == 0)
                return false;

            // Creates and sends the command
            OscMessage commandMessage = GetNewCommandMessage(DisableSubjectNotificationAddress);
            foreach (string subject in subjectsSet)
            {
                activeNotifications.Remove(subject);
                commandMessage.Append(subject);
            }

            return SendCommand(commandMessage);
        }

        public bool SubscribeSubject(MessageDataHandler onSubscribeSubject, params OscdSubscription[] subscriptions)
        {
            // Tests if the client is registered
            if (oscUserID == null)
                return false;

            // Filters the received subjects
            HashSet<string> subjectsSet = GetFilteredSubjects(subscriptions.Select(currSubscription => currSubscription.Subject), SubscribeSubjectAddress);

            if (subjectsSet.Count == 0)
                return false;

            OscMessage commandMessage = GetNewCommandMessage(SubscribeSubjectAddress);
            OscdSubscription subscription;
            foreach (string subject in subjectsSet)
            {
                subscription = subscriptions.First(currSubscription => currSubscription.Subject == subject);

                pendingSubscriptions.Add(subscription.Subject, subscription.Handler);

                commandMessage.Append(subscription.Subject);
                commandMessage.Append(subscriptionClient.CurrentPort);
                commandMessage.Append(subscription.EnableNotification);
            }

            return SendCommand(commandMessage, onSubscribeSubject);
        }

        public bool UnsubscribeSubject(params string[] subjects)
        {
            // Tests if the client is registered 
            if (oscUserID == null)
                return false;

            // Gets a set of non-repetitive and valid subjects
            HashSet<string> subjectsSet = GetSubjectSet(subjects);

            // Excludes the received subjects that aren't activeNotifications
            subjectsSet.IntersectWith(activeSubscriptions.Keys);

            // Excludes the received subjects that are in pending commands
            FilterSubjectsByPendingCommand(subjectsSet, UnsubscribeSubjectAddress);

            if (subjectsSet.Count == 0)
                return false;

            // Creates and sends the command
            OscMessage commandMessage = GetNewCommandMessage(UnsubscribeSubjectAddress);
            foreach (string subject in subjectsSet)
            {
                activeSubscriptions.Remove(subject);
                subscriptionClient.UnRegisterMethod(subject);
                commandMessage.Append(subject);
            }

            return SendCommand(commandMessage);
        }
        #endregion

        #region Command Receivers
        private void CommandResponseReceived(object sender, OscMessageReceivedEventArgs e)
        {
            // Tests if the received message is an error
            if (e.Message.Address == ErrorAddress)
                Console.Error.WriteLine($"Command error received");
            else if (e.Message.Address == SubjectNotificationAddress)
            {
                SubjectNotificationReceived(sender, e);
                return;
            }
            else if (e.Message.Address == SubjectCancellationAddress)
            {
                SubjectCancellationReceived(sender, e);
                return;
            }

#if DEBUG
            Console.WriteLine($"Command response received");
#endif

            // Tries to get the command ID received in the message
            if (!GetCommandId(e.Message, out int commandId))
            {
                Console.Error.WriteLine("Response received without a command identification number.");
                return;
            }

            // Tries to get the pending command associated to the received command ID
            if (!pendingCommands.TryGetValue(commandId, out CommandHandlerData handler))
            {
                Console.Error.WriteLine("Received a command response with an unknown command identification number.");
                return;
            }

            // Redirects the message to the correspondent handler
            if (handler.command.Address == RegisterUserAddress)
                RegisterUserResponseReceived(sender, e, handler);
            else if (handler.command.Address == RegisterSubjectAddress)
                RegisterSubjectResponseReceived(sender, e, handler);
            else if (handler.command.Address == GetAvailableSubjectsAddress)
                GetAvailableSubjectsResponseReceived(sender, e, handler);
            else if (handler.command.Address == SubscribeSubjectAddress)
                SubscribeSubjectResponseReceived(sender, e, handler);
            else
                Console.Error.WriteLine("Received an unknown command response.");
        }

        private void RegisterUserResponseReceived(object sender, OscMessageReceivedEventArgs e, CommandHandlerData commandHandler)
        {
            bool wasSucessful;
            // Tests if the received message is an error
            if (e.Message.Address == ErrorAddress)
            {
                Console.Error.WriteLine("Received error while trying to register a user.");
                wasSucessful = false;
            }
            else
            {
                // Tries to get the user ID received in the message
                if (!GetUserId(e.Message, out string userId))
                {
                    Console.Error.WriteLine("Invalid \"Register User\" response received. No OscUser ID found.");
                    return;
                }

                oscUserID = userId;
                wasSucessful = true;
            }

            // Removes the command from the pending list and calls the user's handler
            pendingCommands.Remove(commandHandler.commandId);
            commandHandler.handler(wasSucessful);
        }

        private void RegisterSubjectResponseReceived(object sender, OscMessageReceivedEventArgs e, CommandHandlerData commandHandler)
        {
            bool wasSucessful;
            KeyValuePair<string, bool>[] resultsArray = null;
            // Tests if the received message is an error
            if (e.Message.Address == ErrorAddress)
            {
                Console.Error.WriteLine("Received error while trying to register subjects.");
                wasSucessful = false;
            }
            // Tests if the number of subjects sent matchs the number of subjects received
            else if (e.Message.Data.Count != commandHandler.command.Data.Count - 1)
            {
                Console.Error.WriteLine("The received message doesn't match the data sent.");
                wasSucessful = false;
            }
            else
            {
                Dictionary<string, bool> results = new Dictionary<string, bool>();
                string dataSent;
                int? dataReceived;
                // Tests if the if a subject was added or not (received data is int or null)
                for (int idx = 1; idx < e.Message.Data.Count; idx++)
                {
                    dataSent = (string)commandHandler.command.Data[idx + 1];
                    dataReceived = (int?)e.Message.Data[idx];
                    if (dataReceived != null)
                        // Adds the subject and the received port to the active subjects list
                        activeSubjects.Add(dataSent, (ushort)dataReceived);
                    // Adds the result to the results list
                    results.Add(dataSent, dataReceived != null);
                }

                wasSucessful = true;
                resultsArray = results.ToArray();
            }

            // Removes the command from the pending list and calls the user's handler
            pendingCommands.Remove(commandHandler.commandId);
            commandHandler.handler(wasSucessful, resultsArray);
        }

        private void GetAvailableSubjectsResponseReceived(object sender, OscMessageReceivedEventArgs e, CommandHandlerData commandHandler)
        {
            bool wasSucessful;
            string[] resultsArray = null;
            // Tests if the received message is an error
            if (e.Message.Address == ErrorAddress)
            {
                Console.Error.WriteLine("Received error while trying to get available subjects.");
                wasSucessful = false;
            }
            else
            {
                IList<object> data = e.Message.Data;
                List<string> results = new List<string>(data.Count - 1);
                for (int idx = 1; idx < data.Count; idx++)
                {
                    if (e.Message.Data[idx] is string)
                        results.Add((string)data[idx]);
                    else
                    {
                        results = null;
                    }
                }

                wasSucessful = results != null;
                if (wasSucessful)
                    resultsArray = results.ToArray();
            }

            // Removes the command from the pending list and calls the user's handler
            pendingCommands.Remove(commandHandler.commandId);
            commandHandler.handler(wasSucessful, resultsArray);
        }

        private void SubscribeSubjectResponseReceived(object sender, OscMessageReceivedEventArgs e, CommandHandlerData commandHandler)
        {
            bool wasSucessful;
            KeyValuePair<string, bool>[] resultsArray = null;

            // Tests if the received message is an error
            if (e.Message.Address == ErrorAddress)
            {
                Console.Error.WriteLine("Received error while trying to subscribe subjects.");
                wasSucessful = false;
            }
            // Tests if the number of subjects sent matchs the number of subjects received
            else if (e.Message.Data.Count != (commandHandler.command.Data.Count + 1) / 3f)
            {
                Console.Error.WriteLine("The received message doesn't match the data sent.");
                wasSucessful = false;
            }
            else
            {
                Dictionary<string, bool> results = new Dictionary<string, bool>();
                string subject;
                bool? wasSubscribed;
                MessageDataHandler handler;
                // Tests if a subject was subscribed or not (received data is bool or null)
                for (int rIdx = 1, sIdx = 2; rIdx < e.Message.Data.Count; rIdx++, sIdx += 3)
                {
                    subject = (string)commandHandler.command.Data[sIdx];
                    wasSubscribed = (bool?)e.Message.Data[rIdx];

                    // Gets and removes the handler added when the command was sent
                    handler = pendingSubscriptions[subject];
                    pendingSubscriptions.Remove(subject);

                    if (wasSubscribed == true)
                    {
                        // Adds the subject and the received port to the active subjects list
                        activeSubscriptions.Add(subject, handler);
                        subscriptionClient.RegisterMethod(subject);
                    }
                    // Tests if the notification of the a subject was activated
                    else if ((bool)commandHandler.command.Data[sIdx + 2])
                        activeNotifications.Add(subject);

                    // Adds the result to the results list
                    results.Add(subject, wasSubscribed == true);
                }

                wasSucessful = true;
                resultsArray = results.ToArray();
            }

            // Removes the command from the pending list and calls the user's handler
            pendingCommands.Remove(commandHandler.commandId);
            commandHandler.handler(wasSucessful, resultsArray);
        }

        private void SubjectNotificationReceived(object sender, OscMessageReceivedEventArgs e)
        {
            string subject;
            if (e.Message.Data.Count != 1 || (subject = e.Message.Data[0] as string) == null ||
                !Regex.IsMatch(subject, subjectRegexPattern) || !activeNotifications.Contains(subject))
                Console.Error.WriteLine("An invalid subject notification alert was received");
            else if (!activeSubscriptions.ContainsKey(subject))
            {
                activeNotifications.Remove(subject);
                OnSubjectNotificationReceived?.Invoke(this, new SubjectAlertReceivedEventArgs(subject));
            }
        }

        private void SubjectCancellationReceived(object sender, OscMessageReceivedEventArgs e)
        {
            string subject;
            if (e.Message.Data.Count != 1 || (subject = e.Message.Data[0] as string) == null || !Regex.IsMatch(subject, subjectRegexPattern))
                Console.Error.WriteLine("An invalid subject cancelation alert was received");
            else if (activeSubscriptions.Remove(subject))
            {
                subscriptionClient.UnRegisterMethod(subject);
                OnSubjectCancellationReceived?.Invoke(this, new SubjectAlertReceivedEventArgs(subject));
            }
        }
        #endregion

        #region Information Getters
        private bool GetCommandId(OscMessage message, out int commandId)
        {
            if (message.Data.Count < 1 || !(message.Data[0] is int))
            {
                commandId = 0;
                return false;
            }

            commandId = (int)message.Data[0];
            return true;
        }

        private bool GetUserId(OscMessage message, out string userId)
        {
            if (message.Data.Count < 2 || !(message.Data[1] is string))
            {
                userId = string.Empty;
                return false;
            }

            userId = (string)message.Data[1];
            if (!Guid.TryParse(userId, out Guid guid))
            {
                userId = string.Empty;
                return false;
            }

            return true;
        }

        private HashSet<string> GetFilteredSubjects(IEnumerable<string> subjects, string pendingCommandAddress)
        {
            // Gets a set of non-repetitive and valid subjects
            HashSet<string> subjectsSet = GetSubjectSet(subjects);

            // Excludes the received subjects that are already active or subscribed
            subjectsSet.ExceptWith(activeSubjects.Keys);
            subjectsSet.ExceptWith(activeSubscriptions.Keys);
            subjectsSet.ExceptWith(activeNotifications);

            // Excludes the received subjects that are in pending commands
            FilterSubjectsByPendingCommand(subjectsSet, pendingCommandAddress);

            return subjectsSet;
        }

        private void FilterSubjectsByPendingCommand(HashSet<string> subjectsSet, string pendingCommandAddress)
        {
            foreach (CommandHandlerData handlerData in pendingCommands.Values.Where(
                            commandValue => commandValue.command.Address == pendingCommandAddress))
                subjectsSet.RemoveWhere(subject => handlerData.command.Data.Contains(subject));
        }

        private static HashSet<string> GetSubjectSet(IEnumerable<string> subjects)
        {
            HashSet<string> subjectsSet = new HashSet<string>(subjects);

            // Tests if any of the received subjects are repeated
            if (subjectsSet.Count() < subjects.Count())
                throw new ArgumentException("Some of the received subjects are repeated");

            // Tests if all the received subjects are valid
            if (subjectsSet.Any(subject => !Regex.IsMatch(subject, subjectRegexPattern)))
                throw new ArgumentException("Some of the received subjects aren't valid");

            return subjectsSet;
        }
        #endregion

        #region Receive Error Receivers
        private void CommandReceiveErrored(object sender, ExceptionEventArgs e)
        {
            Console.Error.WriteLine($"Error during reception of command: {e.Exception.GetType()} ({e.Exception.Message})\n{e.Exception.StackTrace}");
        }

        private void PublicationReceiveErrored(object sender, ExceptionEventArgs e)
        {
            Console.Error.WriteLine($"Error during reception of publication: {e.Exception.GetType()} ({e.Exception.Message})\n{e.Exception.StackTrace}");
        }
        #endregion
    }
}
