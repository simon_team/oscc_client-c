﻿using System;

namespace OSCD.Client
{
    public class OscdSubscription
    {
        public string Subject { get; internal set; }
        public MessageDataHandler Handler { get; internal set; }
        public bool EnableNotification { get; internal set; }

        internal OscdSubscription()
        {
        }

        public OscdSubscription(string subject, MessageDataHandler handler, bool enableNotification)
        {
            if (subject == null || subject == string.Empty || handler == null)
                throw new ArgumentNullException("None of the parameters can be null");

            Subject = subject;
            Handler = handler;
            EnableNotification = enableNotification;
        }
    }
}
