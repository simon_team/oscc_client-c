﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace OSCD.Client.ComTester
{
    class CommunicationTesterClient1 : CommunicationTesterClient
    {
        private int stream1MessageCounter = 0;
        private int stream2MessageCounter = 0;

        private int onSubscriptionReceived_1_Counter = 0;
        private int onSubscriptionReceived_2_Counter = 0;

        public CommunicationTesterClient1(IPEndPoint oscdEndPoint) : base(oscdEndPoint)
        {
        }

        protected override void StartTester()
        {
            Console.WriteLine("---===== OSCDClient Communication Tester 1 =====---");

            subjects.Add("C1S1", "/Client1/Subject1");
            subjects.Add("C1S2", "/Client1/Subject2");

            subjects.Add("C2S1", "/Client2/Subject1");
            subjects.Add("C3S1", "/Client3/Subject1");

            Connect();
            PrepareClient();

            Stream();

            Disconnect();
        }

        protected override void PrepareClient()
        {
            Console.WriteLine($"RegisterUser: {client.RegisterUser(OnRegisterUser)}");
        }

        protected new void OnRegisterUser(object[] data)
        {
            base.OnRegisterUser(data);

            Console.WriteLine($"RegisterSubject: {client.RegisterSubject(OnRegisterSubject, subjects["C1S1"], subjects["C1S2"])}");
        }

        protected new void OnRegisterSubject(object[] data)
        {
            base.OnRegisterSubject(data);

            OscdSubscription c2Subscription1 = new OscdSubscription(subjects["C2S1"], OnSubscriptionReceived_1, false);
            OscdSubscription c3Subscription1 = new OscdSubscription(subjects["C3S1"], OnSubscriptionReceived_2, false);


            Console.WriteLine($"SubscribeSubject: {client.SubscribeSubject(OnSubscribeSubject, c2Subscription1, c3Subscription1)}");
        }

        private void OnSubscriptionReceived_1(object[] data)
        {
            lock (ConsoleWriterLock)
            {
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
                Console.WriteLine($"(1 Received)   ({++onSubscriptionReceived_1_Counter} | {onSubscriptionReceived_2_Counter})");
                Console.ResetColor();
            }
        }

        private void OnSubscriptionReceived_2(object[] data)
        {
            lock (ConsoleWriterLock)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine($"(2 Received)   ({onSubscriptionReceived_1_Counter} | {++onSubscriptionReceived_2_Counter})");
                Console.ResetColor();
            }
        }

        protected override async Task Stream()
        {
            Console.WriteLine("\nPress 'ENTER' to start streaming.");
            Console.ReadLine();

            await Task.Run(() =>
            {
                string subject1 = subjects["C1S1"], subject2 = subjects["C1S2"];
                while (IsStreamLooping)
                {
                    if (random.NextDouble() > .5f)
                    {
                        if (client.Publish(subject1, random.NextDouble(), random.Next(), random.NextDouble() > .5f, subject1))
                            Console.WriteLine($"(1 Sent - {subject1})   ({++stream1MessageCounter} | {stream2MessageCounter})");
                        else
                            Console.WriteLine($"(1 Error)   ({stream1MessageCounter} | {stream2MessageCounter})");
                    }
                    else
                    {
                        if (client.Publish(subject2, random.NextDouble(), random.Next(), random.NextDouble() > .5f, subject2))
                            Console.WriteLine($"(2 Sent - {subject2})   ({stream1MessageCounter} | {++stream2MessageCounter})");
                        else
                            Console.WriteLine($"(2 Error)   ({stream1MessageCounter} | {stream2MessageCounter})");
                    }

                    Thread.Sleep(1000);
                }
            });
        }
    }
}
