﻿using System;
using System.Net;

namespace OSCD.Client.ComTester
{
    class Client
    {
        static void Main(string[] args)
        {
            Console.SetError(new ErrorWriter());
            IPAddress address = AskOscdAddress();
            int value = WriteMenu(address);
            TesterClient client = null;
            IPEndPoint oscdEndPoint = new IPEndPoint(address, 5103);

            switch (value)
            {
                case 1:
                    client = new CommandTesterClient1(oscdEndPoint);
                    break;
                case 2:
                    client = new CommandTesterClient2(oscdEndPoint);
                    break;
                case 3:
                    client = new CommunicationTesterClient1(oscdEndPoint);
                    break;
                case 4:
                    client = new CommunicationTesterClient2(oscdEndPoint);
                    break;
                case 5:
                    client = new CommunicationTesterClient3(oscdEndPoint);
                    break;
                case 6:
                    client = new CommunicationTesterClient4(oscdEndPoint);
                    break;
                case 7:
                    client = new CommunicationTesterClient5(oscdEndPoint);
                    break;
                default:
                    Console.Error.WriteLine("Menu error..");
                    return;
            }

            if (client != null)
                client.Start();

            Console.WriteLine("\nPress 'ENTER' to exit.");
            Console.ReadLine();
        }

        private static IPAddress AskOscdAddress()
        {
            int iter = 0;
            IPAddress address;
            string receivedString;
            do
            {
                Console.Clear();
                if (iter > 0)
                    Console.Error.WriteLine("Invalid Option...");
                Console.Write("Enter the OSC Distributor IP address or 'ENTER' to loopback: ");
                receivedString = Console.ReadLine();

                if (receivedString.Length == 0)
                    receivedString = IPAddress.Loopback.ToString();

                iter++;
            } while (!IPAddress.TryParse(receivedString, out address));

            return address;
        }

        private static int WriteMenu(IPAddress oscdAddress)
        {
            int iter = 0;
            int value;
            string receivedString;
            do
            {
                Console.Clear();
                Console.WriteLine("OSC Distributor IP: " + oscdAddress);
                Console.WriteLine("+-------------------------------------------------------------------------+");
                Console.WriteLine("|                                                                         |");
                Console.WriteLine("|                    OSC Distributor C# Client Tester                     |");
                Console.WriteLine("|                                                                         |");
                Console.WriteLine("| 1: Command Tester Client 1                                              |");
                Console.WriteLine("|    - Registers 4 subjects                                               |");
                Console.WriteLine("|    - Gets the available subjects                                        |");
                Console.WriteLine("|    - Enables notification for 3 (self) subjects                         |");
                Console.WriteLine("|    - Enables notification for 4 subjects of Client 2                    |");
                Console.WriteLine("|    - Registers 3 subjects                                               |");
                Console.WriteLine("|    - Unregisters 1 subject                                              |");
                Console.WriteLine("| 2: Command Tester Client 2                                              |");
                Console.WriteLine("|    - Registers 4 subjects                                               |");
                Console.WriteLine("|    - Subscribes to 5 subjects of Client 1                               |");
                Console.WriteLine("|    - Enables notification for 2 subjects of Client 1                    |");
                Console.WriteLine("|    - Disables notification for 2 subjects of Client 1                   |");
                Console.WriteLine("|    - Unsubscribes to 1 subjects of Client 1                             |");
                Console.WriteLine("|    - Subscribes to 1 subjects of Client 1                               |");
                Console.WriteLine("| 3: Communication Tester Client 1                                        |");
                Console.WriteLine("|    - Registers 2 subjects                                               |");
                Console.WriteLine("|    - Subscribes to the subjects of Client 2 and Client 3                |");
                Console.WriteLine("|    - Publishs in one of them every 1 second                             |");
                Console.WriteLine("| 4: Communication Tester Client 2                                        |");
                Console.WriteLine("|    - Registers 1 subject                                                |");
                Console.WriteLine("|    - Publishs on it as fast as possible                                 |");
                Console.WriteLine("| 5: Communication Tester Client 3                                        |");
                Console.WriteLine("|    - Registers 1 subject                                                |");
                Console.WriteLine("|    - Publishs on it as fast as possible                                 |");
                Console.WriteLine("| 6: Communication Tester Client 4                                        |");
                Console.WriteLine("|    - Subscribes to the subjects of Client 1                             |");
                Console.WriteLine("| 7: Communication Tester Client 5                                        |");
                Console.WriteLine("|    - Registers a given subject                                          |");
                Console.WriteLine("|    - Subscribes to a given subject                                      |");
                Console.WriteLine("|    - Publishs at the specified rate                                     |");
                Console.WriteLine("+-------------------------------------------------------------------------+");

                if (iter > 0)
                    Console.WriteLine("Invalid Option...");
                Console.Write("Select the client to use: ");
                receivedString = Console.ReadLine();

                iter++;
            } while (!int.TryParse(receivedString, out value) || value < 1 || value > 7);
            return value;
        }
    }
}
