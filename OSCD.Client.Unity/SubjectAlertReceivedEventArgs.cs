﻿using System;

namespace OSCD.Client.Unity
{
    public class SubjectAlertReceivedEventArgs : EventArgs
    {
        public string Subject { get; private set; }

        public SubjectAlertReceivedEventArgs(string subject)
        {
            Subject = subject;
        }
    }
}
