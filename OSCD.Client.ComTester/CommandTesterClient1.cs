﻿using System;
using System.Net;

namespace OSCD.Client.ComTester
{
    class CommandTesterClient1 : TesterClient
    {
        public CommandTesterClient1(IPEndPoint oscdEndPoint) : base(oscdEndPoint)
        {
        }

        protected override void StartTester()
        {
            Console.WriteLine("---===== OSCDClient Command Tester 1 =====---");

            subjects.Add("C1S1", "/Client1/Subject1");
            subjects.Add("C1S2", "/Client1/Subject2");
            subjects.Add("C1S3", "/Client1/Subject3");
            subjects.Add("C1S4", "/Client1/Subject4");
            subjects.Add("C1S5", "/Client1/Subject5");
            subjects.Add("C1S6", "/Client1/Subject6");
            subjects.Add("C1S7", "/Client1/Subject7");

            subjects.Add("C2S1", "/Client2/Subject1");
            subjects.Add("C2S2", "/Client2/Subject2");
            subjects.Add("C2S3", "/Client2/Subject3");
            subjects.Add("C2S4", "/Client2/Subject4");

            Connect();
            RegisterUser();
            RegisterSubjects_1();
            GetAvailableSubjects();
            EnableSubjectNotifications();

            Console.WriteLine(string.Format("{0} Change Client {0}", new string('-', 50)));

            RegisterSubjects_2();
            UnregisterSubjects();

            Disconnect();
        }

        #region Tests
        private void RegisterUser()
        {
            Console.WriteLine("\nPress 'ENTER' to register the user. (2x)");
            Console.ReadLine();
            Console.WriteLine($"RegisterUser: {client.RegisterUser(OnRegisterUser)}");
            Console.WriteLine($"RegisterUser: {client.RegisterUser(OnRegisterUser)}");

            Console.WriteLine("\nPress 'ENTER' to register the user.");
            Console.ReadLine();
            Console.WriteLine($"RegisterUser: {client.RegisterUser(OnRegisterUser)}");
        }

        private void RegisterSubjects_1()
        {
            Console.WriteLine($"\nPress 'ENTER' to register the subjects \"{subjects["C1S1"]}\" and \"{subjects["C1S2"]}\". (2x)");
            Console.ReadLine();
            Console.WriteLine($"RegisterSubject: {client.RegisterSubject(OnRegisterSubject, subjects["C1S1"], subjects["C1S2"])}");
            Console.WriteLine($"RegisterSubject: {client.RegisterSubject(OnRegisterSubject, subjects["C1S1"], subjects["C1S2"])}");

            Console.WriteLine($"\nPress 'ENTER' to register the subjects \"{subjects["C1S1"]}\" and \"{subjects["C1S2"]}\".");
            Console.ReadLine();
            Console.WriteLine($"RegisterSubject: {client.RegisterSubject(OnRegisterSubject, subjects["C1S1"], subjects["C1S2"])}");

            Console.WriteLine($"\nPress 'ENTER' to register the subjects \"{subjects["C1S1"]}\", \"{subjects["C1S2"]}\", \"{subjects["C1S3"]}\" and \"{subjects["C1S4"]}\".");
            Console.ReadLine();
            Console.WriteLine($"RegisterSubject: {client.RegisterSubject(OnRegisterSubject, subjects["C1S1"], subjects["C1S2"], subjects["C1S3"], subjects["C1S4"])}");
        }

        private void GetAvailableSubjects()
        {
            Console.WriteLine("\nPress 'ENTER' to get the available subjects. (2x)");
            Console.ReadLine();
            Console.WriteLine($"GetAvailableSubjects: {client.GetAvailableSubjects(OnGetAvailableSubjects)}");
            Console.WriteLine($"GetAvailableSubjects: {client.GetAvailableSubjects(OnGetAvailableSubjects)}");
        }

        private void EnableSubjectNotifications()
        {
            Console.WriteLine($"\nPress 'ENTER' to enable notifications to the subjects \"{subjects["C1S1"]}\", \"{subjects["C1S2"]}\" and \"{subjects["C1S3"]}\".");
            Console.ReadLine();
            Console.WriteLine($"EnableSubjectNotification: {client.EnableSubjectNotification(subjects["C1S1"], subjects["C1S2"], subjects["C1S3"])}");

            Console.WriteLine($"\nPress 'ENTER' to enable notifications to the subjects \"{subjects["C2S1"]}\", \"{subjects["C2S2"]}\" and \"{subjects["C2S3"]}\"");
            Console.ReadLine();
            Console.WriteLine($"EnableSubjectNotification: {client.EnableSubjectNotification(subjects["C2S1"], subjects["C2S2"], subjects["C2S3"])}");

            Console.WriteLine($"\nPress 'ENTER' to enable notifications to the subjects \"{subjects["C2S1"]}\", \"{subjects["C2S2"]}\" and \"{subjects["C2S3"]}\".");
            Console.ReadLine();
            Console.WriteLine($"EnableSubjectNotification: {client.EnableSubjectNotification(subjects["C2S1"], subjects["C2S2"], subjects["C2S3"])}");

            Console.WriteLine($"\nPress 'ENTER' to enable notifications to the subjects \"{subjects["C2S1"]}\", \"{subjects["C2S2"]}\", \"{subjects["C2S3"]}\" and \"{subjects["C2S4"]}\".");
            Console.ReadLine();
            Console.WriteLine($"EnableSubjectNotification: {client.EnableSubjectNotification(subjects["C2S1"], subjects["C2S2"], subjects["C2S3"], subjects["C2S4"])}");
        }

        private void RegisterSubjects_2()
        {
            Console.WriteLine($"\nPress 'ENTER' to register the subjects \"{subjects["C1S5"]}\", \"{subjects["C1S6"]}\" and \"{subjects["C1S7"]}\".");
            Console.ReadLine();
            Console.WriteLine($"RegisterSubject: {client.RegisterSubject(OnRegisterSubject, subjects["C1S5"], subjects["C1S6"], subjects["C1S7"])}");
        }

        private void UnregisterSubjects()
        {
            Console.WriteLine($"\nPress 'ENTER' to unregister the subject \"{subjects["C1S2"]}\".");
            Console.ReadLine();
            Console.WriteLine($"UnregisterSubject: {client.UnregisterSubject(subjects["C1S2"])}");

            Console.WriteLine($"\nPress 'ENTER' to unregister the subject \"{subjects["C1S2"]}\".");
            Console.ReadLine();
            Console.WriteLine($"UnregisterSubject: {client.UnregisterSubject(subjects["C1S2"])}");
        }
        #endregion
    }
}
