﻿using System;
using System.Net;

namespace OSCD.Client.ComTester
{
    class CommandTesterClient2 : TesterClient
    {
        private int onSubscriptionReceived_1_Counter = 0;
        private int onSubscriptionReceived_2_Counter = 0;
        private int onSubscriptionReceived_3_Counter = 0;

        public CommandTesterClient2(IPEndPoint oscdEndPoint) : base(oscdEndPoint)
        {
        }

        protected override void StartTester()
        {
            Console.WriteLine("---===== OSCDClient Command Tester 2 =====---");

            subjects.Add("C1S1", "/Client1/Subject1");
            subjects.Add("C1S2", "/Client1/Subject2");
            subjects.Add("C1S3", "/Client1/Subject3");
            subjects.Add("C1S4", "/Client1/Subject4");
            subjects.Add("C1S5", "/Client1/Subject5");
            subjects.Add("C1S6", "/Client1/Subject6");
            subjects.Add("C1S7", "/Client1/Subject7");

            subjects.Add("C2S1", "/Client2/Subject1");
            subjects.Add("C2S2", "/Client2/Subject2");
            subjects.Add("C2S3", "/Client2/Subject3");
            subjects.Add("C2S4", "/Client2/Subject4");

            Connect();
            RegisterUser();
            RegisterSubject();
            SubscribeSubject_1();
            EnableSubjectNotification();
            DisableSubjectNotification();

            Console.WriteLine(string.Format("{0} Change Client {0}", new string('-', 50)));

            UnsubscribeSubject();
            SubscribeSubject_2();

            Console.WriteLine(string.Format("{0} Change Client {0}", new string('-', 50)));

            Disconnect();
        }

        #region Tests
        private void RegisterUser()
        {
            Console.WriteLine("\nPress 'ENTER' to register the user.");
            Console.ReadLine();
            Console.WriteLine($"RegisterUser: {client.RegisterUser(OnRegisterUser)}");
        }

        private void RegisterSubject()
        {
            Console.WriteLine($"\nPress 'ENTER' to register the subjects \"{subjects["C2S1"]}\", \"{subjects["C2S2"]}\", \"{subjects["C2S3"]}\" and \"{subjects["C2S4"]}\".");
            Console.ReadLine();
            Console.WriteLine($"RegisterSubject: {client.RegisterSubject(OnRegisterSubject, subjects["C2S1"], subjects["C2S2"], subjects["C2S3"], subjects["C2S4"])}");
        }

        private void SubscribeSubject_1()
        {
            OscdSubscription c1Subscription1 = new OscdSubscription(subjects["C1S1"], OnSubscriptionReceived_1, false);
            OscdSubscription c1Subscription2 = new OscdSubscription(subjects["C1S2"], OnSubscriptionReceived_2, false);
            OscdSubscription c1Subscription5 = new OscdSubscription(subjects["C1S5"], OnSubscriptionReceived_3, true);
            OscdSubscription c1Subscription6 = new OscdSubscription(subjects["C1S6"], OnSubscriptionReceived_3, true);
            OscdSubscription c1Subscription7 = new OscdSubscription(subjects["C1S7"], OnSubscriptionReceived_3, false);

            Console.WriteLine($"\nPress 'ENTER' to subscribe subjects \"{subjects["C1S1"]}\", \"{subjects["C1S2"]}\", \"{subjects["C1S5"]}\", \"{subjects["C1S6"]}\" and \"{subjects["C1S7"]}\".");
            Console.ReadLine();
            Console.WriteLine($"SubscribeSubject: {client.SubscribeSubject(OnSubscribeSubject, c1Subscription1, c1Subscription2, c1Subscription5, c1Subscription6, c1Subscription7)}");
        }

        private void EnableSubjectNotification()
        {
            Console.WriteLine($"\nPress 'ENTER' to enable notifications to the subjects \"{subjects["C1S3"]}\" and \"{subjects["C1S4"]}\".");
            Console.ReadLine();
            Console.WriteLine($"EnableSubjectNotification: {client.EnableSubjectNotification(subjects["C1S3"], subjects["C1S4"])}");
        }

        private void DisableSubjectNotification()
        {
            Console.WriteLine($"\nPress 'ENTER' to disable notifications to the subjects \"{subjects["C1S6"]}\" and \"{subjects["C1S7"]}\". (2x)");
            Console.ReadLine();
            Console.WriteLine($"DisableSubjectNotification: {client.DisableSubjectNotification(subjects["C1S6"], subjects["C1S7"])}");
            Console.WriteLine($"DisableSubjectNotification: {client.DisableSubjectNotification(subjects["C1S6"], subjects["C1S7"])}");

            Console.WriteLine($"\nPress 'ENTER' to disable notifications to the subject \"{subjects["C1S6"]}\". ");
            Console.ReadLine();
            Console.WriteLine($"DisableSubjectNotification: {client.DisableSubjectNotification(subjects["C1S6"])}");
        }

        private void UnsubscribeSubject()
        {
            Console.WriteLine($"\nPress 'ENTER' to unsubscribe the subject \"{subjects["C1S1"]}\". (2x)");
            Console.ReadLine();
            Console.WriteLine($"UnsubscribeSubject: {client.UnsubscribeSubject(subjects["C1S1"])}");
            Console.WriteLine($"UnsubscribeSubject: {client.UnsubscribeSubject(subjects["C1S1"])}");

            Console.WriteLine($"\nPress 'ENTER' to unsubscribe the subject \"{subjects["C1S1"]}\".");
            Console.ReadLine();
            Console.WriteLine($"UnsubscribeSubject: {client.UnsubscribeSubject(subjects["C1S1"])}");
        }

        private void SubscribeSubject_2()
        {
            OscdSubscription c1Subscription1 = new OscdSubscription(subjects["C1S1"], OnSubscriptionReceived_1, false);

            Console.WriteLine($"\nPress 'ENTER' to subscribe the subject \"{subjects["C1S1"]}\".");
            Console.ReadLine();
            Console.WriteLine($"SubscribeSubject: {client.SubscribeSubject(OnSubscribeSubject, c1Subscription1)}");

            Console.WriteLine($"\nPress 'ENTER' to subscribe the subject \"{subjects["C1S1"]}\".");
            Console.ReadLine();
            Console.WriteLine($"SubscribeSubject: {client.SubscribeSubject(OnSubscribeSubject, c1Subscription1)}");
        }
        #endregion

        #region Publication Handlers
        private void OnSubscriptionReceived_1(object[] data)
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine($"({++onSubscriptionReceived_1_Counter}) 1: Received message with {data.Length} elements");
            Console.ResetColor();
        }

        private void OnSubscriptionReceived_2(object[] data)
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine($"({++onSubscriptionReceived_2_Counter}) 2: Received message with {data.Length} elements");
            Console.ResetColor();
        }

        private void OnSubscriptionReceived_3(object[] data)
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine($"({++onSubscriptionReceived_3_Counter}) 3: Received message with {data.Length} elements");
            Console.ResetColor();
        }
        #endregion
    }
}
