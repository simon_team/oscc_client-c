﻿using Bespoke.Common.Osc;
using System;
using System.Diagnostics;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace OSCD.Client.ComTester
{
    class CommunicationTesterClient5 : CommunicationTesterClient
    {
        const string baseOSCDAddress = "/PerformanceTest/";
        const int writePeriod = 1;

        private bool canSubscribe = false;

        private int streamFrequency;
        private int streamDuration;
        private DateTime streamStartTime;
        private int streamPacketSize;

        private int pubSucessMessageCounter = 0;
        private int pubMessageCounter = 0;
        private int subMessageCounter = 0;

        private int lastPubSucessMessageCounter, lastPubMessageCounter, lastSubMessageCounter;


        private bool IsWriterLooping = true;

        private Task consoleWriter;

        public CommunicationTesterClient5(IPEndPoint oscdEndPoint) : base(oscdEndPoint)
        {

        }

        protected override void StartTester()
        {
            Console.WriteLine("---===== OSCDClient Communication Tester 5 =====---");

            string receivedString;
            do
            {
                Console.Write("Insert the publication subject: ");
                receivedString = baseOSCDAddress + Console.ReadLine();
            }
            while (!Regex.IsMatch(receivedString, OscdClient.subjectRegexPattern));
            subjects.Add("pub", receivedString);

            do
            {
                Console.Write("Insert the subscription subject: ");
                receivedString = baseOSCDAddress + Console.ReadLine();
            }
            while (!Regex.IsMatch(receivedString, OscdClient.subjectRegexPattern));
            subjects.Add("sub", receivedString);

            do
            {
                Console.Write("Insert the stream frequency (in Hertz): ");
                receivedString = Console.ReadLine();
            }
            while (!int.TryParse(receivedString, out streamFrequency));

            do
            {
                Console.Write("Insert the stream duration (in seconds): ");
                receivedString = Console.ReadLine();
            }
            while (!int.TryParse(receivedString, out streamDuration));

            do
            {
                Console.Write("Insert the message size (in bytes): ");
                receivedString = Console.ReadLine();
            }
            while (!int.TryParse(receivedString, out streamPacketSize));

            Connect();
            PrepareClient();

            StartConsoleWriter();

            Stream();

            Disconnect();
        }

        protected override void PrepareClient()
        {
            Console.WriteLine($"RegisterUser: {client.RegisterUser(OnRegisterUser)}");

            do
            {
                Thread.Yield();
            } while (!canSubscribe);

            Subscribe();
        }

        protected new void OnRegisterUser(object[] data)
        {
            base.OnRegisterUser(data);

            Console.WriteLine($"RegisterSubject ({subjects["pub"]}): {client.RegisterSubject(OnRegisterSubject, subjects["pub"])}");
        }

        protected new void OnRegisterSubject(object[] data)
        {
            base.OnRegisterSubject(data);
            canSubscribe = true;
        }

        private void Subscribe()
        {
            OscdSubscription subscription = new OscdSubscription(subjects["sub"], OnSubscriptionReceived, false);

            Console.WriteLine($"\nPress 'ENTER' to subscribe subject \"{subscription.Subject}\".");
            Console.ReadLine();
            Console.WriteLine($"SubscribeSubject ({subscription.Subject}): {client.SubscribeSubject(OnSubscribeSubject, subscription)}");
        }

        private void StartConsoleWriter()
        {
            consoleWriter = Task.Run(() =>
            {
                double pubPercentage = 0;
                bool write = false;
                while (IsWriterLooping)
                {
                    Thread.Sleep(writePeriod * 1000);

                    if (lastPubSucessMessageCounter != pubSucessMessageCounter)
                    {
                        lastPubSucessMessageCounter = pubSucessMessageCounter;
                        pubPercentage = Math.Round(lastPubSucessMessageCounter * 100f / lastPubMessageCounter, 3);
                        write = true;
                    }

                    if (lastPubMessageCounter != pubMessageCounter)
                    {
                        lastPubMessageCounter = pubMessageCounter;
                        pubPercentage = Math.Round(lastPubSucessMessageCounter * 100f / lastPubMessageCounter, 3);
                        write = true;
                    }

                    if (lastSubMessageCounter != subMessageCounter)
                    {
                        lastSubMessageCounter = subMessageCounter;
                        write = true;
                    }

                    if (write)
                        Console.WriteLine($"({(DateTime.Now - streamStartTime).ToString("c")}) Published: {lastPubSucessMessageCounter} of {lastPubMessageCounter} ({pubPercentage}%) | Received: {lastSubMessageCounter}");

                    write = false;
                }
            });
        }

        protected override async Task Stream()
        {
            Console.WriteLine("\nPress 'ENTER' to start streaming.");
            Console.ReadLine();

            await Task.Run(() =>
            {
                string subject = subjects["pub"];
                streamStartTime = DateTime.Now;
                DateTime lastRunTime;
                long streamTickWait = TimeSpan.TicksPerSecond / streamFrequency;
                string padString = new string('a', streamPacketSize - 82);

                while (IsStreamLooping && (DateTime.Now - streamStartTime).TotalSeconds < streamDuration)
                {
                    Interlocked.Increment(ref pubMessageCounter);
                    if (client.Publish(subject, random.NextDouble(), random.Next(), random.NextDouble() > .5f, padString))
                        Interlocked.Increment(ref pubSucessMessageCounter);
                    lastRunTime = DateTime.Now;

                    //Thread.Sleep((int)Math.Round(1000f / streamFrequency));
                    //SpinWait.SpinUntil(() => { return (DateTime.Now - lastRunTime).Ticks >= streamTickSpin; });
                    while ((DateTime.Now - lastRunTime).Ticks < streamTickWait) ;
                }
            });

            Console.WriteLine("Stream stopped.");
        }

        private void OnSubscriptionReceived(object[] data)
        {
            Interlocked.Increment(ref subMessageCounter);
        }

        public new void Stop()
        {
            base.Stop();

            IsWriterLooping = false;
        }
    }
}
